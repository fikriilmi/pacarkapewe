import unittest
import time
from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index, profile
from .forms import form_status
from .models import Status
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options


# Create your tests here.

class Lab5UnitTest(TestCase):
	#ini untuk test url masuk
	def test_url_is_exist(self):
		response = Client().get("")
		self.assertEqual(response.status_code, 200)
 	
 	#ini untuk cek views apakah ada index 
	def test_using_index_function(self):
		found = resolve('/')
		self.assertEqual(found.func, index)
 
 	#ini untuk url ga masuk
	def test_url_is_not_exist(self):
		response = Client().get("/wawa")
		self.assertEqual(response.status_code, 404)

	#ini untuk mencek apakah templete yg keluar adalah index.html
	def test_if_using_index_template(self):
		response = Client().get('')
		self.assertTemplateUsed(response, 'index.html')

	#test untuk inputan nya required
	def test_form_validation_for_blank_items(self):
		form = form_status(data={"status" : ""})
		self.assertFalse(form.is_valid())
		self.assertEqual(
			form.errors["status"],
			["This field is required."]
		)

	#test ini untuk cek apakah ouputnya keluar apa kaga
	def test_lab5_post_success_and_render_the_result(self):
		test = 'Anonymous'
		response_post = Client().post('/', {"status" : test})
		self.assertEqual(response_post.status_code, 302)
		
		response= Client().get('/')
		html_response = response.content.decode('utf8')
		self.assertIn(test, html_response)
	
	#test 
	def test_lab5_post_error_and_render_the_result(self):
		test = 'Anonymous'
		response_post = Client().post('/', {"status" : ""})
		self.assertEqual(response_post.status_code, 200)
		
		response= Client().get('/')
		html_response = response.content.decode('utf8')
		self.assertNotIn(test, html_response)
	

	#ini untuk mencek apakah masuk database
	def test_model_in_database(self):
		# Creating a new activity
		new_activity = Status.objects.create(status = 'abeng ganteng parah' )
		# Retrieving all available activity
		counting_all_available_todo = Status.objects.all().count()

		self.assertEqual(counting_all_available_todo, 1)


class challange(TestCase):
	def test_if_hello_is_exist(self):
		test = "abeng ganteng parah"
		response= Client().get('/profile/')
		html_response = response.content.decode('utf8')
		self.assertIn(test, html_response)

class S7_functional_test(TestCase):

	def setUp(self):
		chrome_options = Options()
		chrome_options.add_argument('--dns-prefetch-disable')
		chrome_options.add_argument('--no-sandbox')
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('disable-gpu')
		self.browser = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
		super(S7_functional_test,self).setUp()

	def tearDown(self):
		self.browser.implicitly_wait(5)
		self.browser.quit()
		super(S7_functional_test,self).tearDown()

	def test_input_status(self):
		selenium = self.browser
		selenium.get('http://localhost:8000')

		status_form = selenium.find_element_by_id('id_status')
		submit = selenium.find_element_by_id('id_submit')

		status_form.send_keys('coba coba')
		submit.send_keys(Keys.RETURN)

		self.assertIn('coba coba', self.browser.page_source)



		

