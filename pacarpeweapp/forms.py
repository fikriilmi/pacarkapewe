from django import forms
from .models import Status

class form_status(forms.ModelForm):
	status = forms.CharField(max_length = 300)

	class Meta :
		model = Status
		fields = ["status"]