
from .forms import form_status
from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from pacarpeweapp.models import Status
from . import forms


# Create your views here.
def index(request):
	if request.method == "POST":
		form = forms.form_status(request.POST)
		if form.is_valid():
			form.save()
			return redirect('index')
	else:
		form = forms.form_status()

	status_list = Status.objects.all()
	return render(request, 'index.html', {'form': form , "Status" : status_list })


def profile (request):
	return render(request, "profile.html")